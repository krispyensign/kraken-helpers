import { AddOrder, AssetPair, CancelOrder, CancelOrderStatus, Publication, Subscribe, SubscriptionStatus, Ticker, Unsubscribe, AddOrderStatus } from 'exchange-models/kraken';
import { ExchangePair, OrderPairs, PairPriceUpdate } from 'exchange-models/exchange';
export interface DoneWorking {
    event: 'done';
}
export interface Dictionary<T> {
    [key: string]: T;
}
export declare function compareTypes<U>(o: object, ...propertyNames: (keyof U)[]): boolean | string | undefined;
export declare type AssetPairsResponse = [string, Partial<AssetPair>][];
export declare type AssetTicksResponse = ResponseWrapper<Dictionary<Partial<Ticker>>>;
export interface ResponseWrapper<T = object> {
    error: string[];
    result: T;
}
export declare function isLastTick(pairName: string, tick?: Partial<Ticker>): tick is Ticker;
export declare function isKrakenPair(pairName: string, pair?: Partial<AssetPair>): pair is AssetPair;
export declare function isTicker(payload: object): payload is Ticker;
export declare function isPublication(event: object): event is Publication;
export declare function isError(err: unknown): err is Error;
export declare function isResponseWrapper(obj: unknown): obj is ResponseWrapper;
export declare function isWork(event: unknown): event is AddOrder | CancelOrder | OrderPairs | DoneWorking;
export declare function createTickSubRequest(instruments: string[]): Subscribe;
export declare function createExchangePair(pairName: string, index: number, pair: AssetPair, tick: Ticker): ExchangePair;
export declare function createStopRequest(): Unsubscribe;
export declare function sleep(ms: number): Promise<unknown>;
export declare function parseEvent(tickData?: string): Error | string | PairPriceUpdate;
export declare function getAvailablePairs(krakenApiUrl: string, threshold: number): Promise<Error | ExchangePair[]>;
export declare function isStatusEvent(event: unknown): event is AddOrderStatus | CancelOrderStatus | SubscriptionStatus;
export declare function makeAuthCall<T = object>(url: string, request: string, nonce: number, apiKey: string, apiPrivateKey: string): Promise<Error | ResponseWrapper<T>>;
export declare function getToken(apiUrl: string, apiKey: string, apiPrivateKey: string): Promise<Error | string>;
