"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getToken = exports.makeAuthCall = exports.isStatusEvent = exports.getAvailablePairs = exports.parseEvent = exports.sleep = exports.createStopRequest = exports.createExchangePair = exports.createTickSubRequest = exports.isWork = exports.isResponseWrapper = exports.isError = exports.isPublication = exports.isTicker = exports.isKrakenPair = exports.isLastTick = exports.compareTypes = void 0;
// eslint-disable-next-line @typescript-eslint/no-var-requires
const crypto = require('crypto');
const dist_1 = require("socket-comms-libs/dist");
const got_1 = __importDefault(require("got"));
const qs_1 = __importDefault(require("qs"));
let krakenTickerPath = '/0/public/Ticker';
let krakenPairsPath = '/0/public/AssetPairs';
let krakenTokenPath = '/0/private/GetWebSocketsToken';
function compareTypes(o, ...propertyNames) {
    // check if object is undefined
    if (!o)
        return undefined;
    // loop through supplied propertynames
    for (let prop of propertyNames) {
        // if property is not in object then return that property
        if (!(prop in o))
            return prop.toString();
    }
    // return true if all properties requested are on object
    return true;
}
exports.compareTypes = compareTypes;
///////////////////////////////////////////////////////////////////////////////
// Kraken type guards
///////////////////////////////////////////////////////////////////////////////
function isLastTick(pairName, tick) {
    if (!tick)
        return false;
    let result = compareTypes(tick, 'a', 'b', 't');
    if (!result)
        throw Error(`Failed to correctly populate tick ${pairName}.`);
    if (typeof result === 'string')
        throw Error(`Missing resource ${result} on pair ${pairName}.`);
    return true;
}
exports.isLastTick = isLastTick;
function isKrakenPair(pairName, pair) {
    if (!pair)
        return false;
    let result = compareTypes(pair, 'wsname', 'base', 'quote', 'fees_maker', 'fees', 'pair_decimals');
    if (!result)
        throw Error(`Failed to correctly populate pair ${pairName}`);
    if (typeof result === 'string')
        throw Error(`Missing resource ${result} on pair ${pairName}.`);
    return true;
}
exports.isKrakenPair = isKrakenPair;
function isTicker(payload) {
    if (!payload)
        return false;
    let result = compareTypes(payload, 'a', 'b', 'c', 'v', 'p', 't', 'l', 'h', 'o');
    if (!result || typeof result === 'string')
        return false;
    return result;
}
exports.isTicker = isTicker;
function isPublication(event) {
    return event.length !== undefined && event.length === 4;
}
exports.isPublication = isPublication;
function isError(err) {
    return (typeof err === 'object' &&
        err.message !== undefined &&
        err.stack !== undefined);
}
exports.isError = isError;
function isResponseWrapper(obj) {
    return typeof obj === 'object' && obj.result !== undefined;
}
exports.isResponseWrapper = isResponseWrapper;
function isWork(event) {
    if (typeof event !== 'object')
        return false;
    let typedEvent = event;
    return (typedEvent === null || typedEvent === void 0 ? void 0 : typedEvent.event) !== undefined;
}
exports.isWork = isWork;
///////////////////////////////////////////////////////////////////////////////
// Kraken type helpers
///////////////////////////////////////////////////////////////////////////////
function createTickSubRequest(instruments) {
    return {
        event: 'subscribe',
        pair: instruments,
        subscription: {
            name: 'ticker',
        },
    };
}
exports.createTickSubRequest = createTickSubRequest;
function createExchangePair(pairName, index, pair, tick) {
    // construct a Pair
    return {
        index: index,
        tradename: pair.wsname,
        name: pairName,
        decimals: pair.pair_decimals,
        baseName: pair.base,
        quoteName: pair.quote,
        makerFee: Number(pair.fees_maker[0][1]) / 100,
        takerFee: Number(pair.fees[0][1]) / 100,
        volume: tick.t[0],
        ask: tick.a[0],
        bid: tick.b[0],
        ordermin: Number(pair.ordermin),
    };
}
exports.createExchangePair = createExchangePair;
function createStopRequest() {
    return {
        event: 'unsubscribe',
        subscription: {
            name: '*',
        },
    };
}
exports.createStopRequest = createStopRequest;
///////////////////////////////////////////////////////////////////////////////
// Kraken tasks
///////////////////////////////////////////////////////////////////////////////
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
exports.sleep = sleep;
function parseEvent(tickData) {
    var _a, _b;
    // make sure we got something if not failure during ws message
    if (!tickData)
        return new Error('TickData missing. Cannot parse.');
    // parse it
    let event = JSON.parse(tickData);
    // check to make sure its not an error.  Something wrong with code itself
    // so need to hard error on this one
    if ('errorMessage' in event)
        throw event.errorMessage;
    // if its not a publication (unlikely) return the tick as a string for logging
    if (!isPublication(event))
        return tickData;
    // split out the publication to the pair and the payload
    let pair = event[3];
    let payload = event[1];
    // check if the payload is a ticker if so then return back an update object
    if (isTicker(payload))
        return {
            tradeName: pair,
            ask: (_a = payload.a) === null || _a === void 0 ? void 0 : _a[0],
            bid: (_b = payload.b) === null || _b === void 0 ? void 0 : _b[0],
        };
    // for now return all other publications as strings for logging
    return tickData;
}
exports.parseEvent = parseEvent;
async function getAvailablePairs(krakenApiUrl, threshold) {
    // get the tradeable asset pairs
    let assetPairsResult = await dist_1.HttpClient.getJson(krakenApiUrl + krakenPairsPath);
    if (isError(assetPairsResult))
        return assetPairsResult;
    // parse the tradeable assetPairs into tuples of name/assetPair
    let assetPairs = Object.entries(assetPairsResult.result);
    // get the last tick for each asset pair
    let assetPairTicksResult = await dist_1.HttpClient.getJson(krakenApiUrl + krakenTickerPath + '?pair=' + assetPairs.map(pair => pair[0]).join(','));
    if (isError(assetPairTicksResult))
        return assetPairTicksResult;
    // rename for easy reading
    let assetPairTicks = assetPairTicksResult.result;
    return (assetPairs
        // skip those pairs that do not support websocket streaming
        // and skip those pairs whose t value is greater than threshold
        // additionally skip all pairs that were not parseable
        .filter(([name, pair]) => {
        var _a, _b;
        return pair.wsname &&
            isKrakenPair(name, pair) &&
            isLastTick(name, assetPairTicks[name]) &&
            ((_b = (_a = assetPairTicks[name]) === null || _a === void 0 ? void 0 : _a.t) === null || _b === void 0 ? void 0 : _b[0]) !== undefined &&
            assetPairTicks[name].t[0] > threshold;
    })
        // convert from array of kraken pairs to exchange pairs
        .map(([name, pair], index) => createExchangePair(name, index, pair, assetPairTicks[name])));
}
exports.getAvailablePairs = getAvailablePairs;
function checkError(response) {
    var _a;
    // if there wasn't a response then bomb
    if (!response) {
        return new Error('Failed to get response back from exchange api!');
    }
    // check if there was an error
    if (((_a = response.error) === null || _a === void 0 ? void 0 : _a.length) > 0) {
        return new Error(response.error
            .filter(e => e.startsWith('E'))
            .map(e => e.substr(1))
            .join(','));
    }
    // pass through if no error
    return response;
}
function getMessageSignature(path, message, secret, nonce) {
    return crypto
        .createHmac('sha512', Buffer.from(secret, 'base64'))
        .update(path +
        crypto
            .createHash('sha256')
            .update(nonce + message)
            .digest('latin1'), 'latin1')
        .digest('base64');
}
function isStatusEvent(event) {
    if (typeof event !== 'object')
        return false;
    let typedEvent = event;
    return (typedEvent === null || typedEvent === void 0 ? void 0 : typedEvent.event) !== undefined && typedEvent.reqid !== undefined;
}
exports.isStatusEvent = isStatusEvent;
async function makeAuthCall(url, request, nonce, apiKey, apiPrivateKey) {
    let gotOptions = {
        url: url,
        headers: {
            'API-Key': apiKey,
            'API-Sign': getMessageSignature(krakenTokenPath, request, apiPrivateKey, nonce),
        },
        timeout: 50000,
        method: 'POST',
        responseType: 'json',
        body: request,
        isStream: false,
    };
    return checkError(await got_1.default.post(gotOptions).json());
}
exports.makeAuthCall = makeAuthCall;
async function getToken(apiUrl, apiKey, apiPrivateKey) {
    let n = new Date().getTime() * 1000;
    let requestData = {
        nonce: n,
    };
    let response = await makeAuthCall(apiUrl + krakenTokenPath, qs_1.default.stringify(requestData), requestData.nonce, apiKey, apiPrivateKey);
    if (isError(response))
        return response;
    return response.result.token;
}
exports.getToken = getToken;
//# sourceMappingURL=index.js.map