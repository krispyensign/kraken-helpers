// eslint-disable-next-line @typescript-eslint/no-var-requires
const crypto = require('crypto')
import { HttpClient } from 'socket-comms-libs/dist'
import {
  AddOrder,
  AssetPair,
  CancelOrder,
  CancelOrderStatus,
  Publication,
  Subscribe,
  SubscriptionStatus,
  Ticker,
  Unsubscribe,
  Token,
  AddOrderStatus,
} from 'exchange-models/kraken'
import { ExchangePair, OrderPairs, PairPriceUpdate } from 'exchange-models/exchange'
import got, { OptionsOfJSONResponseBody } from 'got'
import qs from 'qs'

let krakenTickerPath = '/0/public/Ticker'
let krakenPairsPath = '/0/public/AssetPairs'
let krakenTokenPath = '/0/private/GetWebSocketsToken'

///////////////////////////////////////////////////////////////////////////////
// Common type helpers
///////////////////////////////////////////////////////////////////////////////
export interface DoneWorking {
  event: 'done'
}

export interface Dictionary<T> {
  [key: string]: T
}

export function compareTypes<U>(
  o: object,
  ...propertyNames: (keyof U)[]
): boolean | string | undefined {
  // check if object is undefined
  if (!o) return undefined
  // loop through supplied propertynames
  for (let prop of propertyNames) {
    // if property is not in object then return that property
    if (!(prop in o)) return prop.toString()
  }
  // return true if all properties requested are on object
  return true
}

///////////////////////////////////////////////////////////////////////////////
// Kraken additional types
///////////////////////////////////////////////////////////////////////////////
export type AssetPairsResponse = [string, Partial<AssetPair>][]
export type AssetTicksResponse = ResponseWrapper<Dictionary<Partial<Ticker>>>

export interface ResponseWrapper<T = object> {
  error: string[]
  result: T
}

///////////////////////////////////////////////////////////////////////////////
// Kraken type guards
///////////////////////////////////////////////////////////////////////////////
export function isLastTick(pairName: string, tick?: Partial<Ticker>): tick is Ticker {
  if (!tick) return false
  let result = compareTypes(tick, 'a', 'b', 't')
  if (!result) throw Error(`Failed to correctly populate tick ${pairName}.`)
  if (typeof result === 'string') throw Error(`Missing resource ${result} on pair ${pairName}.`)
  return true
}

export function isKrakenPair(pairName: string, pair?: Partial<AssetPair>): pair is AssetPair {
  if (!pair) return false
  let result = compareTypes(pair, 'wsname', 'base', 'quote', 'fees_maker', 'fees', 'pair_decimals')
  if (!result) throw Error(`Failed to correctly populate pair ${pairName}`)
  if (typeof result === 'string') throw Error(`Missing resource ${result} on pair ${pairName}.`)
  return true
}

export function isTicker(payload: object): payload is Ticker {
  if (!payload) return false
  let result = compareTypes<Ticker>(payload, 'a', 'b', 'c', 'v', 'p', 't', 'l', 'h', 'o')
  if (!result || typeof result === 'string') return false
  return result
}

export function isPublication(event: object): event is Publication {
  return (event as Publication).length !== undefined && (event as Publication).length === 4
}

export function isError(err: unknown): err is Error {
  return (
    typeof err === 'object' &&
    (err as Error).message !== undefined &&
    (err as Error).stack !== undefined
  )
}

export function isResponseWrapper(obj: unknown): obj is ResponseWrapper {
  return typeof obj === 'object' && (obj as ResponseWrapper).result !== undefined
}

export function isWork(event: unknown): event is AddOrder | CancelOrder | OrderPairs | DoneWorking {
  if (typeof event !== 'object') return false
  let typedEvent = event as AddOrder | CancelOrder | OrderPairs | DoneWorking
  return typedEvent?.event !== undefined
}

///////////////////////////////////////////////////////////////////////////////
// Kraken type helpers
///////////////////////////////////////////////////////////////////////////////
export function createTickSubRequest(instruments: string[]): Subscribe {
  return {
    event: 'subscribe',
    pair: instruments,
    subscription: {
      name: 'ticker',
    },
  }
}

export function createExchangePair(
  pairName: string,
  index: number,
  pair: AssetPair,
  tick: Ticker
): ExchangePair {
  // construct a Pair
  return {
    index: index,
    tradename: pair.wsname,
    name: pairName,
    decimals: pair.pair_decimals,
    baseName: pair.base,
    quoteName: pair.quote,
    makerFee: Number(pair.fees_maker[0][1]) / 100,
    takerFee: Number(pair.fees[0][1]) / 100,
    volume: tick.t[0],
    ask: tick.a[0],
    bid: tick.b[0],
    ordermin: Number(pair.ordermin),
  }
}

export function createStopRequest(): Unsubscribe {
  return {
    event: 'unsubscribe',
    subscription: {
      name: '*',
    },
  }
}

///////////////////////////////////////////////////////////////////////////////
// Kraken tasks
///////////////////////////////////////////////////////////////////////////////
export function sleep(ms: number): Promise<unknown> {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export function parseEvent(tickData?: string): Error | string | PairPriceUpdate {
  // make sure we got something if not failure during ws message
  if (!tickData) return new Error('TickData missing. Cannot parse.')

  // parse it
  let event = JSON.parse(tickData)

  // check to make sure its not an error.  Something wrong with code itself
  // so need to hard error on this one
  if ('errorMessage' in event) throw event.errorMessage

  // if its not a publication (unlikely) return the tick as a string for logging
  if (!isPublication(event)) return tickData

  // split out the publication to the pair and the payload
  let pair = event[3]
  let payload = event[1]

  // check if the payload is a ticker if so then return back an update object
  if (isTicker(payload))
    return {
      tradeName: pair,
      ask: payload.a?.[0], // ask price
      bid: payload.b?.[0], // bid price
    }

  // for now return all other publications as strings for logging
  return tickData
}

export async function getAvailablePairs(
  krakenApiUrl: string,
  threshold: number
): Promise<Error | ExchangePair[]> {
  // get the tradeable asset pairs
  let assetPairsResult = await HttpClient.getJson<ResponseWrapper>(krakenApiUrl + krakenPairsPath)

  if (isError(assetPairsResult)) return assetPairsResult

  // parse the tradeable assetPairs into tuples of name/assetPair
  let assetPairs = Object.entries(assetPairsResult.result) as AssetPairsResponse

  // get the last tick for each asset pair
  let assetPairTicksResult = await HttpClient.getJson<AssetTicksResponse>(
    krakenApiUrl + krakenTickerPath + '?pair=' + assetPairs.map(pair => pair[0]).join(',')
  )
  if (isError(assetPairTicksResult)) return assetPairTicksResult

  // rename for easy reading
  let assetPairTicks = assetPairTicksResult.result

  return (
    assetPairs

      // skip those pairs that do not support websocket streaming
      // and skip those pairs whose t value is greater than threshold
      // additionally skip all pairs that were not parseable
      .filter(
        ([name, pair]) =>
          pair.wsname &&
          isKrakenPair(name, pair) &&
          isLastTick(name, assetPairTicks[name]) &&
          assetPairTicks[name]?.t?.[0] !== undefined &&
          assetPairTicks[name].t![0] > threshold
      )

      // convert from array of kraken pairs to exchange pairs
      .map(([name, pair], index) =>
        createExchangePair(name, index, pair as AssetPair, assetPairTicks[name] as Ticker)
      )
  )
}

function checkError<T = object>(response?: ResponseWrapper<T>): Error | ResponseWrapper<T> {
  // if there wasn't a response then bomb
  if (!response) {
    return new Error('Failed to get response back from exchange api!')
  }

  // check if there was an error
  if (response.error?.length > 0) {
    return new Error(
      response.error
        .filter(e => e.startsWith('E'))
        .map(e => e.substr(1))
        .join(',')
    )
  }

  // pass through if no error
  return response
}

function getMessageSignature(path: string, message: string, secret: string, nonce: number): string {
  return crypto
    .createHmac('sha512', Buffer.from(secret, 'base64'))
    .update(
      path +
        crypto
          .createHash('sha256')
          .update(nonce + message)
          .digest('latin1'),
      'latin1'
    )
    .digest('base64')
}

export function isStatusEvent(
  event: unknown
): event is AddOrderStatus | CancelOrderStatus | SubscriptionStatus {
  if (typeof event !== 'object') return false
  let typedEvent = event as AddOrderStatus | CancelOrderStatus | SubscriptionStatus
  return typedEvent?.event !== undefined && typedEvent.reqid !== undefined
}

export async function makeAuthCall<T = object>(
  url: string,
  request: string,
  nonce: number,
  apiKey: string,
  apiPrivateKey: string
): Promise<Error | ResponseWrapper<T>> {
  let gotOptions: OptionsOfJSONResponseBody = {
    url: url,
    headers: {
      'API-Key': apiKey,
      'API-Sign': getMessageSignature(krakenTokenPath, request, apiPrivateKey, nonce),
    },
    timeout: 50000,
    method: 'POST',
    responseType: 'json',
    body: request,
    isStream: false,
  }

  return checkError(await got.post(gotOptions).json<ResponseWrapper<T>>())
}

export async function getToken(
  apiUrl: string,
  apiKey: string,
  apiPrivateKey: string
): Promise<Error | string> {
  let n = new Date().getTime() * 1000
  let requestData = {
    nonce: n,
  }

  let response = await makeAuthCall<Token>(
    apiUrl + krakenTokenPath,
    qs.stringify(requestData),
    requestData.nonce,
    apiKey,
    apiPrivateKey
  )
  if (isError(response)) return response

  return response.result.token
}
